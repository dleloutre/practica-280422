class Wallet {
    constructor(coin) {
        this.coin = coin;
        this.balance = 0;
    }

    getCoin() {
        return this.coin;
    }

    getBalance() {
        return this.balance;
    }

    receiveDeposit(deposit) {
        this.balance += deposit;
    }

    retrieve() {
        this.balance = 0;
    }
}

module.exports = Wallet