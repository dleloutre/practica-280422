class Monitor {
    constructor(){
        this.minValue = 0;
        this.maxValue = 0;
        this.value;
    }
    newValue(value) {
        if (value>this.maxValue) {
            this.maxValue = value
        } 
        if (value<this.minValue){
            this.minValue = value
        }
        this.value = value
    }

    informCoinValue() {
        return this.value
    }
    informCoinMaxValue() {
        return this.maxValue
    }
}

module.exports = Monitor