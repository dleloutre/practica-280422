// import {Wallet} from "./Wallet"
const Wallet = require("./Wallet.js")
const Monitor = require("./Monitor.js")
const { expect } = require('expect');
const sum = require('./sum');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('create wallet with initial deposit', () => {
    const wallet = new Wallet('dogcoin');
    expect(wallet.getCoin()).toBe('dogcoin');
    expect(wallet.getBalance()).toBe(0);
});

test('new deposit increases balance', () => {
    const wallet = new Wallet('dogcoin');
    wallet.receiveDeposit(5);
    expect(wallet.getBalance()).toBe(5);
})

test('retrieve money decreases balance to 0', () => {
    const wallet = new Wallet('dogcoin');
    wallet.retrieve();
    expect(wallet.getBalance()).toBe(0);
})

test('monitor informs current coin value', () => {
    const monitor = new Monitor()
    monitor.newValue(200)
    expect(monitor.informCoinValue()).toBe(200);
})

test('monitor informs current coin value with value 100', () => {
    const monitor = new Monitor()
    monitor.newValue(100)
    expect(monitor.informCoinValue()).toBe(100);
})

test('monitor identifies maximum value', () => {
    const monitor = new Monitor()
    monitor.newValue(110)
    monitor.newValue(100)
    expect(monitor.informCoinMaxValue()).toBe(110);
})